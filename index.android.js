
import React, { Component } from 'react';
import {
  AppRegistry,
  View
} from 'react-native';
import ViewShot from "react-native-view-shot";
import resolveAssetSource from 'resolveAssetSource';

import Container from './components/Container'

export default class overlay extends Component {
  
  render() {
    return (
      <View>
        <Container />
      </View>
    );
  }
}

AppRegistry.registerComponent('overlay', () => overlay);
