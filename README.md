# README #

### What is this repository for? ###

##### What can it do? #####
The user can make his own meme by submitting an URL, adding text and then capture the image to his camera roll.

##### Why was it made? #####
This is a repo for a meme generator. It was made as a research and development project. The original briefing was: 
React Native Photo Editor R&D:

+ Open image or take a new picture
+ Crop image
+ Resize image
+ Add filter (example: greyscale)
+ Custom overlay (example: vector image)
+ Move overlay 
+ Scale overlay 
+ Save image

### The good, the bad and the future ###

##### What went well #####

Being one of my first React projects I think structuring the code to React conventions turned out pretty good. 
I had trouble fitting the uploaded image to the full width of the device while maintaining aspect ratio at the same time. This proved to be hard to do in React, but I eventually succeeded.

Hooking up the Viewshot package, which manages capturing the image and its overlay text was surprisingly easy. 

##### What went bad #####
When starting with this project I at first tried to add instagram filter like functionality. I tried to achieve this trough [gl-react-native](https://github.com/gre/gl-react-native-v2). This package allows for Open GL functionality in react native. 
At first it looked like a perfect solution, but soon I realised that it took a lot of Open GL knowledge to write shaders for it. This would have cost me a lot of time. It took me 4 days to add a saturation filter, a brightness filter and a vibrancy filter. 
Besides that, the package contains some weird bugs. for example, at first images would not even render because I had to set the outer container to opacity 0.001. Furthermore elements refused to render on top of each other. 
The above made me abandon adding filter functionality altogether. 

##### The future #####
These features need to be implemented in the future
+ This project needs the actual overlay functionality. This could be achieved the same way I already implemented the text overlay. 
+ resizing the image
+ Maybe the react gl package is better in the future
+ Making memes out of your own photos


### How to run: ###

1. Pull project
2. npm install
3. react-native run-android
4. profit?

