import React, {Component} from 'react';
import {
   StyleSheet,
   Text,
   View,
   Image,
   Dimensions,
   Button,
   TextInput,
   Slider,
   ScrollView,
} from 'react-native';
import ViewShot from "react-native-view-shot";

import PictureFrame from './PictureFrame';
import Picture from './Picture';
import ControlContainer from './ControlContainer';
import ControlItem from './ControlItem'

class Container extends Component {
   constructor(props) {
    super(props);
    this.state = {
      text: 'GENERATE THAT JUICY MEME',
      textY: 10,
      image: 'https://imgflip.com/s/meme/One-Does-Not-Simply.jpg',
      imageWidth: 100,
      imageHeight: 100,
      imageUpdatedHeight: 300,
    }
  }

  render() {
    return (
      <View >
         <PictureFrame ref='pictureFrame'>
            <Picture url={this.state.image} />
            <Text 
               style={[styles.text, {top: this.state.textY} ]}
            >
               {this.state.text}
            </Text>
         </PictureFrame>

         <Button 
          title={'Capture'} 
          onPress={() => { this.refs.pictureFrame.captureMe()}}>
         </Button>

         <ControlContainer>
            <ControlItem>
               <Text>Image Url</Text>
               <TextInput 
                  value={this.state.image}
                  onChangeText={(image) => this.setState({image})}
               />
            </ControlItem>

            <ControlItem>
               <Text>Text</Text>
               <TextInput 
                  value={this.state.text} 
                  onChangeText={(text) => this.setState({text})}
               />
            </ControlItem>
            <ControlItem>
               <Text>Text position Y</Text>
               <Slider 
                  minimumValue={0} 
                  maximumValue={300} 
                  onValueChange={(textY) => this.setState({textY})} 
               />
            </ControlItem>
         </ControlContainer>
      </View>


    );
  }

}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },

  text: {
    color:'white', 
    position: 'absolute',
    zIndex: 9, 
    textAlign: 'center', 
    alignSelf: 'center',
    fontSize: 40,
    width: Dimensions.width

  }
});
export default Container;