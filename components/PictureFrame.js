import React, {Component} from 'react';
import {
   View,
   CameraRoll
} from 'react-native';
import ViewShot from "react-native-view-shot";

class PictureFrame extends Component {
   constructor (props) {
      super(props);
   }

  captureMe () {
    this.refs.viewShot.capture().then(uri => {
      CameraRoll.saveToCameraRoll(uri);
    });
  }
   render () {
      return (
         <View> 
            <ViewShot ref='viewShot'>
               {this.props.children}
            </ViewShot>
         </View>
         )
      }
}

export default PictureFrame;