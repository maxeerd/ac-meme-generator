import React, {Component} from 'react';
import {
   Image,
   View,
   StyleSheet,
   Dimensions
} from 'react-native';

class Picture extends Component {

   constructor(props) {
      super(props);
      this.state = {
      imageWidth: 100,
      imageHeight: 100,
      imageUpdatedHeight: 300,
      }
   }

   onLoad = () => {
      Image.getSize(this.props.url , (width, height) => {
         this.setState({imageWidth: width, imageHeight: height});
         this.updateImageDimensions();
      })
   }

   updateImageDimensions() {
      aspectratio = (this.state.imageHeight / this.state.imageWidth) ;
      newHeight = aspectratio * Dimensions.get('window').width;
      console.log(newHeight); 
      this.setState({imageUpdatedHeight: newHeight})
   }

   render () {
      console.log(this.props)
      return (
         <View>
            <Image
               resizeMode={'contain'} 
               style={styles.image, {height: this.state.imageUpdatedHeight}}  
               source={{ uri: this.props.url }}
               onLoad={this.onLoad} 
            />
         </View>
         )
   }
}
const styles = StyleSheet.create({
   image: {
      width: Dimensions.width,
      height: 300,
      alignSelf: 'stretch',
   },
})
export default Picture;