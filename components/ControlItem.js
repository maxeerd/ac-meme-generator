import React, {Component} from 'react';
import {
   View,
   StyleSheet
} from 'react-native';

const ControlItem = (props) => {
   return (
      <View style={styles.container}>
         {props.children}
      </View>

      )


}
const styles = StyleSheet.create({
   container : {
      borderBottomWidth: 1,
      borderBottomColor: 'black',
      marginBottom: 10
   },
})
export default ControlItem;